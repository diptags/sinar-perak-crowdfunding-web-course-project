from django.urls import path
from donation.views import donationPage, donationShowPage, donationAction, donationAPI

# app_name = "donation_form"

urlpatterns = [
    path('', donationPage.as_view(), name="donation_form"),
    path('show', donationShowPage, name="donation_page"),
    path('donation-api/<str:email>', donationAPI, name="donation-api"),
    path('mulai-donasi/', donationAction.as_view(), name='mulai-donasi'),
]
