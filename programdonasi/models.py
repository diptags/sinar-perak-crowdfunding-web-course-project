from django.db import models
from django.urls import reverse

# Create your models here.
class ProgramModels(models.Model):
	nama = models.CharField(max_length = 50)
	target = models.BigIntegerField()
	total = models.BigIntegerField()
	deskripsi = models.TextField()
	image_url = models.CharField(max_length=5000, 
		default="https://preview.ibb.co/iNNFY0/background.jpg")

	def __str__(self):
		return self.nama

	def get_absolute_url(self):
		return reverse('details', kwargs={'my_id': self.id})