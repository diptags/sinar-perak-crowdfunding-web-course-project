from django.urls import path

from crowdfunding import views

urlpatterns = [
    path('', views.home_page, name='home_page'),
    path('logout/', views.logout_action, name='logout'),
    path('login-page/', views.login_page, name='login-page'),
    path('testimony-api/', views.testimony_api, name='testimony-api'),
    path('about/', views.about, name='about'),
]
